/*
Capstone PNP Project
Skyler Sommer
version 1.2
1.0 contains servcies.csv & hosts.csv data from manual metasploit query.
1.0 All csv are uploaded using winscp to sommers@webpages.sou.edu/home/SOU/sommers/
1.1 Creates user table to store user ID, Name, and password
1.1 Still need to associate userID with pnpHosts & pnpServices
1.2 Added constraints for primary and foreign keys
*/

/* selects webpages.sou.edu sql database */
USE usr_sommers_0;
/* drop tables in dependency order */
Drop table if exists pnpHosts;
Drop table if exists pnpServices;
Drop table if exists pnpUser;
/* creates table to hold pnp User data. Password is stored as md5 hash */
CREATE TABLE pnpUser (
	userID VARCHAR(16) not null,
	userPassword CHAR(32)not null,
	constraint pnpUserPK primary key(userID)
) engine=innodb ;
/* creates tables to hold metasploit services.csv data */
CREATE TABLE pnpServices (
	ipAddress VARCHAR(15)not null,
	portNum INT(5)not null,
	portType VARCHAR(3)not null,
	portName VARCHAR(40),
	portState VARCHAR(6),
	portInfo VARCHAR(60),
	constraint pnpServicesPK primary key(ipAddress,portNum)
) engine=innodb ;
/* creates tables to hold metasploit hosts.csv data */
CREATE TABLE pnpHosts (
	ipAddress VARCHAR(15)not null,
	macAddress CHAR(18)not null,
	hostName VARCHAR(40),
	hostOSname VARCHAR(40),
	hostOSversion VARCHAR(40),
	hostOSservicePack VARCHAR(40),
	hostType VARCHAR(60),
	hostInfo VARCHAR(60),
	HostComments VARCHAR(60),
	constraint pnpHostsPK primary key(ipAddress,macAddress),
	constraint pnpHostsFK foreign key(ipAddress) references pnpServices(ipAddress)
) engine=innodb ;
/* Loads metasploit services.csv data ignoring the first row*/
LOAD DATA LOCAL INFILE '/services.csv' 
INTO TABLE pnpServices
FIELDS TERMINATED BY ','
ENCLOSED BY '\"' 
LINES TERMINATED BY '\n'
IGNORE 1 LINES (
	ipAddress,
	portNum,
	portType,
	portName,
	portState,
	portInfo
);
/* Loads metasploit hosts.csv data ignoring the first row*/
LOAD DATA LOCAL INFILE '/hosts.csv' 
INTO TABLE pnpHosts
FIELDS TERMINATED BY ','
ENCLOSED BY '\"' 
LINES TERMINATED BY '\n'
IGNORE 1 LINES (
	ipAddress,
	macAddress,
	hostName,
	hostOSname,
	hostOSversion,
	hostOSservicePack,
	hostType,
	hostInfo,
	HostComments
);
/* inserts pnpUser dummy data */
insert into pnpUser values ('Ackler', '5f4dcc3b5aa765d61d8327deb882cf99');
/* returns new tables*/
SELECT * FROM usr_sommers_0.pnpServices;
SELECT * FROM usr_sommers_0.pnpHosts;
SELECT * FROM usr_sommers_0.pnpUser;