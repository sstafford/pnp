#!/bin/bash

mask2cidr() {
	nbits=0
	IFS=.
	for dec in $1 ; do
		case $dec in
			255) let nbits+=8;;
			254) let nbits+=7;;
			252) let nbits+=6;;
			248) let nbits+=5;;
			240) let nbits+=4;;
			224) let nbits+=3;;
			192) let nbits+=2;;
			128) let nbits+=1;;
			0);;
			*) echo "Error: $dec is not recognized"; exit 1
		esac
	done
	echo "$nbits"
}

# Get ip address and mask from ifconfig
ip=$(ifconfig wlan1 | grep 'inet addr:' | cut -d: -f2 | awk '{print $1}')
mask=$(ifconfig wlan1 | grep 'Mask:' | awk 'BEGIN {FS="Mask:"}; {print $2}')

# Turn mask into cidr notation
numbits=$(mask2cidr $mask)
ipcidrmask="$ip/$numbits"
echo $ipcidrmask

# Run nmap
nmap -n -sS -O -PT80 $ipcidrmask -oA inventory > nmapout


exit 0
